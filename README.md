Serial and parallel implementations of the VizRank algorithm.

VizRank is composed of a few different algorithms, such as k-NN and ReliefF. 
A parallel impementation of both algorithms is also provided to allow for different 
execution scenarios (where different steps of the VizRank algorithm are paralelized), 
to allow performance comparison.

Implementations are written in C++ with CUDA framework support. Linked libraries 
(eg. OpenCV) are not provided in the repository due to storage requirements.