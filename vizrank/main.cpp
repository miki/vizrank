#include "format.hpp"
#include "Main.h"

int k;

knn* getKNNManager(int k, int knn_alg){
	switch (knn_alg){
	case 0:
		return &KnnOpenCv(k);
	case 1:
		return &KnnFlannCPU(k);
	case 2:
		return &fsknn(k);
	case 3:
		return &KnnFlannGPU(k);
	}
}

Data* scatter(Data& data, int f1, int f2){
	cout << boost::format("Scattering data: features %1% - %2%") % f1 % f2;
	
	float x1, x2, y;
	Data* spData = new Data(data.samples, 2);
	spData->targets = data.targets;
	spData->samples = data.samples;
	spData->features = 2;

	//data.X.copy(data.X.get_ptr(0, f1), spData.X.get_ptr(0, 0), data.X.rows, data.X.cols);

	// Build a scatterplot
	for (int k = 0; k < spData->samples; k++){
		spData->X.set(k, 0, data.X.get(k, f1));
		spData->X.set(k, 1, data.X.get(k, f2));
		spData->Y.at(k) = data.Y.at(k);
	}

	cout << "\t\t ... done\n";

	return spData;
}

// TODO: needs refacto (knn::run returns predictions for all pairs of features)
//vector<float> scoreScatterPlots(Data& data){
//	vector<float> scores;
//	float score, x1, x2, y;
//	knn* knnManager = getKNNManager(k, 0);
//	Matrix<float>& predictions = knnManager->run(data);
//
//	// For each pair of features
//	for (int i = 0; i < data.features; i++){
//		for (int j = 0; j < i; j++){
//			scores.push_back(score);
//		}
//	}
//
//	knnManager->release();
//
//	return scores;
//}
