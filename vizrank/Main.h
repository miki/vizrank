#include "knn.h"
#include "flann/KnnFlannGPU.h"
#include "flann/KnnFlannCPU.h"
#include "opencv/KnnOpenCV.h"
#include "fsknn/fsknn.h"
#include "bruteForce/KnnBruteForce.h"

vector<float> scoreScatterPlots(Data* data);
knn* getKNNManager(int k, int knn_alg);
Data* scatter(Data& data, int f1, int f2);
