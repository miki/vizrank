#pragma once

#define FLANN_USE_CUDA

#include <flann\flann.hpp>

#include "../knn.h"

using namespace std;

class KnnFlannCPU : virtual public knn
{
public:

	KnnFlannCPU(int k) : knn(k) {
	}

	~KnnFlannCPU() { }
	
	void KnnFlannCPU::buildPredictions(Matrix<float>& predictions, flann::Matrix<int>& neighbours);
	virtual void run(Data& data, Matrix<float>& predictions);
	virtual void release();
};
