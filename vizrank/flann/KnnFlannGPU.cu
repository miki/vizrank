#include "KnnFlannGPU.h"

void KnnFlannGPU::run(Data& data, Matrix<float>& predictions) {
	flann::Matrix<float> dataset(data.X.ptr(), data.features, data.samples);
	flann::Matrix<float> query(data.X.ptr(), data.features, data.samples);
	flann::Matrix<int> indices(new int[query.rows*K], query.rows, K);
	flann::Matrix<float> dists(new float[query.rows*K], query.rows, K);

	//// construct an randomized kd-tree index using 4 kd-trees
	//flann::KDTreeCuda3dIndex<flann::L2<float>> index(dataset, flann::KDTreeCuda3dIndexParams());
	//index.buildIndex();
	//// do a knn search, using 128 checks
	//index.knnSearch(query, indices, dists, K, flann::SearchParams(128));
	//
	delete[] dataset.ptr();

	delete[] query.ptr();
	delete[] indices.ptr();
	delete[] dists.ptr();
}

void KnnFlannGPU::release(){
	data.release();
}
