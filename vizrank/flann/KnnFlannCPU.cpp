#include "KnnFlannCPU.h"

void KnnFlannCPU::buildPredictions(Matrix<float>& predictions, flann::Matrix<int>& neighbours){
	int* counter;

	// for each sample
	for (int j = 0; j < neighbours.rows; j++)
	{
		counter = (int*)calloc(data.targets, sizeof(int));
		
		// count class occurences
		for (int i = 0; i < K; i++)
		{
			int* index = new int(*(neighbours.operator[](j) + 1));
			counter[*index] += 1;
		}

		// save specific class probability
		for (int i = 0; i < K; i++)
		{
			predictions.set(j, i, ((float)counter[i]) / K);
		}
	}
}

void KnnFlannCPU::run(Data& data, Matrix<float>& predictions) {
	knn::data = data;
	flann::Matrix<float> dataset(data.X.ptr(), data.samples, data.features);

	
	//flann::Index<flann::L2<float>> index(dataset, flann::KDTreeIndexParams(4));


	//index.buildIndex();
	flann::Matrix<float> query(data.X.ptr(), data.samples, data.features);
	flann::Matrix<int> indices(new int[query.rows*K], query.rows, K);
	flann::Matrix<float> dists(new float[query.rows*K], query.rows, K);

	////index.knnSearch(query, indices, dists, K, flann::SearchParams(128));

	buildPredictions(predictions, indices);

	delete[] query.ptr();
	delete[] indices.ptr();
	delete[] dists.ptr();
}

void KnnFlannCPU::release(){
}
