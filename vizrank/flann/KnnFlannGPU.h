#pragma once

#define FLANN_USE_CUDA
#include <flann\flann.hpp>
#include <flann\algorithms\kdtree_cuda_3d_index.h>

#include "../knn.h"

using namespace std;

class KnnFlannGPU : virtual public knn
{
public:
	KnnFlannGPU(int k) : knn(k) {
	}

	~KnnFlannGPU() { }

	virtual void run(Data& data, Matrix<float>& predictions);
	virtual void release();
};
