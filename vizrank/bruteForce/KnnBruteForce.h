#include "../knn.h"

class KnnBruteForce : virtual public knn
{
private:

public:
	KnnBruteForce(int k) : knn(k) {
	}

	~KnnBruteForce() { release(); };

	virtual void release();
	virtual void run(Data& data, Matrix<float>& predictions);
};