#include "KnnBruteForce.h"
#include <iostream>

void KnnBruteForce::run(Data& data, Matrix<float>& predictions) {
	cout << "\nRunning KnnBruteForce";

	int *P, i, j, k, p1, p2, score, max_k, *counter;
	float *X, *D, dist, d1, d2, distance, *prediction;

	D = (float*)malloc(K * 2 * sizeof(float));
	X = data.X.data;

	prediction = predictions.data;
	
	/* for each example i */
	for (i = 0; i < data.samples; i++) {
		/* reset D, which is used to keep track of nearest neighbors */
		for (k = 0; k < K; k++) {
			D[k * 2] = 2e38;
		}
		/* for each example j */
		for (j = 0; j < data.samples; j++) {
			if (i != j) {
				/* find max_k: the neighbor in D that is farthest from example i */
				max_k = 0;
				for (k = 0; k < K; k++) {
					if (D[k * 2] > D[max_k * 2]) {
						max_k = k;
					}
				}
				/* compute the distance between i and j */
				d1 = data.X.get(i, 0) - data.X.get(j, 0);
				d2 = data.X.get(i, 1) - data.X.get(j, 1);
				dist = sqrtf(d1 * d1 + d2 * d2);
				if (dist < D[max_k * 2]) {
					D[max_k * 2] = dist;
					D[max_k * 2 + 1] = data.Y[j];
				}
			}
		}

		// calculate specific class probabilities
		counter = (int*)calloc(data.targets, sizeof(int));
		for (int k = 0; k < K; k++)
		{
			counter[(int)D[k * 2 + 1] - 1] += 1;
		}

		for (int k = 0; k < data.targets; k++)
		{
			predictions.set(i, k, ((float)counter[k]) / K);
		}
	}

	cout << "\t\t ... done\n";
}

void KnnBruteForce::release(){

}
