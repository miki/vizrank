#define CATCH_CONFIG_MAIN
#include "Catch.hpp"
#include "knn.h"
#include "Data.h"
#include "Main.h"

Data* _data = 0;
Data* getData(){
	if (_data == NULL){
		_data = new Data("../data_big.dat");
	}

	return _data;
}

void assertKnn(Data data, Matrix<float> predictions){
	REQUIRE(predictions.cols == data.targets);
	REQUIRE(predictions.rows == data.samples);
}

TEST_CASE("Load data") {
	Data data = *getData();
	REQUIRE(data.features == 50);
	REQUIRE(data.samples == 10000);
	REQUIRE(data.targets == 3);
	REQUIRE(data.X.get(0, 0) > 0.0f);
	REQUIRE(data.X.get(1, 13) > 0.0f);
	REQUIRE(data.X.get(7, 0)> 0.0f);
}

TEST_CASE("k-NN - bruteforce CPU") {
	int k = 15;
	knn* knnManager = &KnnBruteForce(k);

	REQUIRE(knnManager->K == k);
	Data data = *getData();
	Data subData = *scatter(data, 2, 5);
	Matrix<float> predictions = Matrix<float>(data.samples, data.targets);

	knnManager->run(subData, predictions);
	assertKnn(data, predictions);
}

TEST_CASE("k-NN - opencv") {
	int k = 15;
	knn* knnManager = &KnnOpenCv(k);

	REQUIRE(knnManager->K == k);
	Data data = *getData();
	Data subData = *scatter(data, 2, 5);
	Matrix<float> predictions = Matrix<float>(data.samples, data.targets);

	knnManager->run(subData, predictions);
	assertKnn(data, predictions);
}

TEST_CASE("k-NN - flann CPU") {
	int k = 3;
	knn* knnManager = &KnnFlannCPU(k);

	REQUIRE(knnManager->K == k);
	Data data = *getData();
	Matrix<float> predictions = Matrix<float>(data.samples, data.targets);

	//knnManager->run(data, predictions);
	//assertKnn(data, predictions);
}

TEST_CASE("k-NN - flann GPU") {
	int k = 3;
	knn* knnManager = &KnnFlannGPU(k);

	REQUIRE(knnManager->K == k);
	Data data = *getData();
	Matrix<float> predictions = Matrix<float>(data.samples, data.targets);

	//knnManager->run(data, predictions);
}

TEST_CASE("k-NN - fsknn") {
	int k = 10;
	knn* knnManager = &fsknn(k);

	REQUIRE(knnManager->K == k);

	Data data = *getData();
	Data subData = *scatter(data, 2, 5);

	Matrix<float> predictions = Matrix<float>(data.samples, data.targets);

	knnManager->run(subData, predictions);
}

TEST_CASE("scatter data") {
	Data data = *getData();
	Data data2 = *scatter(data, 2, 5);

	REQUIRE(data2.features == 2);
	REQUIRE(data2.targets == data.targets);
	REQUIRE(data2.samples == data.samples);

	REQUIRE(data2.Y.at(3) == data.Y.at(3));
	REQUIRE(data2.X.get(3, 0) == data.X.get(3, 2));
	REQUIRE(data2.X.get(3, 1) == data.X.get(3, 5));
}

TEST_CASE("calculate brier (one vector)") {
	int k = 2;
	Data data = Data("../data_small.dat");
	knn* knnManager = &KnnOpenCv(k);

	float probs[] = { 0.8, 0.2 };

	float score = knnManager->brier(probs, 1, data.targets);

	REQUIRE(score == 0.04f);
}

TEST_CASE("calculate brier (matrix)") {
	int k = 2;
	Data data = Data("../data_small.dat");
	knn* knnManager = &KnnOpenCv(k);
	float probs[] = { 0.8, 0.2, 0.7, 0.3, 0.75, 0.25, 0.9, 0.1, 0.81, 0.19,	//first 5 samples (class 1)
		0.22, 0.78, 0.33, 0.67, 0.27, 0.73, 0.15, 0.85, 0.18, 0.82 };		//second 5 samples (class 2)

	Matrix<float> predictions = Matrix<float>(probs, 5, 2);

	float score = knnManager->brierScore(data, predictions);
	REQUIRE(score == 0.05237f);
}

TEST_CASE("test matrix row getter") {
	Data data = Data("../data_small.dat");
	REQUIRE(*data.X.row(0) == 1.0f);
	REQUIRE(*(data.X.row(6) + 2) == 12.0f);
}