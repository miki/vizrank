#pragma once

#include "Data.h"
#include <string>

using namespace std;

class knn
{
protected:
	Data data;

public:
	int K;

	knn(){

	}

	knn(int k){
		K = k;
	}

	/*
		in:	sample data
		out: predictions (prob. vector for each class)
	*/
	virtual void run(Data& data, Matrix<float>& predictions) = 0;

	virtual void release() = 0;
	
	virtual float score(Matrix<float>& neighbours) {
		return 0;
	}
	
	/*
		in: 
			sample data, 
			vector of kNN predictions (class probabilities)
		out: score
	*/
	float brierScore(Data& data, Matrix<float>& predictions){
		float truth, score = 0;

		for (int i = 0; i < data.samples; i++){
			truth = data.Y.at(i);
			score += brier(predictions.row(i), truth, data.targets);
		}

		return score / data.samples;
	}

	float brier(float* pVector, float truth, int targets){
		float score = 0, prob = 0;

		for (int i = 0; i < targets; i++){
			prob = *(pVector + i);
			score += i + 1 == truth ? pow(prob - 1, 2) : pow(prob, 2);
		}

		return score / targets;
	}
};
