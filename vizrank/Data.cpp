#include "format.hpp"
#include "Data.h"
#include "opencv2\opencv.hpp"
#include <fstream>
#include <string>
#include <sstream>
#include <iostream>

using namespace std;

Data::Data() {}

Data::Data(string path){
	loadData(path);
}

Data::Data(int s, int ft){
	samples = s;
	features = ft;
	X = Matrix<float>(samples, features);
	Y = vector<float>(samples);
}

Data::~Data() {

}

vector<string> &_split(const string &s, char delim, vector<string> &elems) {
	stringstream ss(s);
	string item;
	while (getline(ss, item, delim)) {
		elems.push_back(item);
	}

	return elems;
}

vector<string> _split(const string &s, char delim) {
	vector<string> elems;
	_split(s, delim, elems);
	return elems;
}

void Data::loadData(string file) {
	cout << boost::format("Loading file %1% ") % file;

	ifstream infile(file);
	string line;
	int pass = -1;

	while (getline(infile, line))
	{
		string data;
		vector<string> x = _split(line, ';');

		if (pass < 0) {
			samples = atoi(x.at(0).c_str());
			features = atoi(x.at(1).c_str());
			targets = atoi(x.at(2).c_str());
			
			Y.reserve(samples);
			X = Matrix<float>(samples, features);
		} else {
			for (int i = 0; i < x.size(); i++){
				if (i == features){
					Y.push_back(atof(x.at(i).c_str()));
				}
				else {
					X.set(pass, i, atof(x.at(i).c_str()));
				}
			}
		}
		
		pass++;
	}

	infile.close();

	cout << boost::format("\t\t ... done\n");
}

void Data::dispMat(vector<vector<float>> N) {
	int rows = N.size();
	int cols = N.at(0).size();

	for (int i = 0; i < rows; i++){
		for (int j = 0; j < cols; j++){
			printf("%.2f \t", N.at(i).at(j));
		}

		cout << "\n";
	}
}

void Data::release(){
	Y.clear();
	Y.shrink_to_fit();
}