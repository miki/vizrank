#ifndef DataH
#define DataH
#include <string>
#include <vector>

using namespace std;

#pragma once

template <typename T>
class Matrix
{
public:
	typedef T type;

	int stride;
	int cols;
	int rows;
	T* data;

	Matrix(){}
	Matrix(int rows_, int cols_){
		rows = rows_;
		cols = cols_;
		data = (T*)malloc(rows_ * cols_ * sizeof(T));
		stride = sizeof(T) * cols;
	}

	Matrix(T* data_, int rows_, int cols_){
		data = data_;
		rows = rows_;
		cols = cols_;
		stride = sizeof(T) * cols;
	}

	T* row(int row_) {
		return data + row_ * cols;
	}

	T get(int row_, int col_) {
		return *ptr(row_, col_);
	}

	void set(int row_, int col_, T value) {
		*ptr(row_, col_) = value;
	}

	T* add(int row_, int col_, T value){
		T* val = ptr(row_, col_);
		*val += value;

		return val;
	}

	void copy(T* src, T* dest, int count, int offset) {
		while (count--) {
			*dest = *src;
			dest += offset;
			src += offset;
		}
	}

	T* ptr() const {
		return data;
	}

	T* ptr(int row_, int col_) {
		return data + ((row_ * cols) + col_);
	}

	T* ptr(int row_) {
		return data + (row_ * cols) ;
	}

	int size() {
		return (rows + 1) * cols;
	}

	~Matrix() {}
};

class Data
{
public:
	int features;
	int samples;

	// we assume a class set of discrete integer values,
	// spanning from 1 to <targets>
	int targets;

	vector<float> Y;
	Matrix<float> X;

	Data();
	Data(string file);
	Data(int samples, int features);
	~Data();

	void loadData(string file);
	void release();

	static void dispMat(vector<vector<float>> N);
};

#endif