#pragma once

#include "opencv2\opencv.hpp"
#include "../knn.h"

class KnnOpenCv : virtual public knn
{
private:
	Matrix<float>* toVector(cv::Mat& mat);
	cv::Mat* toMat(Matrix<float>& v);
	cv::Mat* toMat(vector<float>& v);
	void buildPredictions(cv::Mat neighbours, Matrix<float>& predictions);

public:
	cv::Mat* X;
	cv::Mat* Y;
	cv::Mat* X_test;

	KnnOpenCv(int k) : knn(k) {
	}

	~KnnOpenCv() { release(); };

	virtual void release();
	virtual void run(Data& data, Matrix<float>& predictions);
};
