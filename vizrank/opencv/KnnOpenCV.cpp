#include "opencv2\ml\ml.hpp"
#include "opencv2\core\core.hpp"
#include <stdio.h>
#include <iostream>
#include "../Data.h"
#include "KnnOpenCV.h"

using namespace std;
Matrix<float>* KnnOpenCv::toVector(cv::Mat& mat) {
	return &Matrix<float>(&mat.at<float>(0), mat.rows, mat.cols);
}

cv::Mat* KnnOpenCv::toMat(Matrix<float>& v) {
	return new cv::Mat(v.rows, v.cols, CV_32F, v.data);
}

cv::Mat* KnnOpenCv::toMat(vector<float>& v) {
	return new cv::Mat(v.size(), 1, CV_32F, &v.at(0));
}

void KnnOpenCv::buildPredictions(cv::Mat neighbours, Matrix<float>& predictions){
	int* counter;

	//for each sample
	for (int j = 0; j < neighbours.rows; j++)
	{
		counter = (int*)calloc(data.targets, sizeof(int));

		// count class occurences
		for (int i = 0; i < K; i++)
		{
			counter[(int) neighbours.at<float>(j, i) - 1] += 1;
		}

		// save specific class probabilities
		for (int i = 0; i < data.targets; i++)
		{
			predictions.set(j, i, ((float) counter[i]) / K);
		}
	}
}

void KnnOpenCv::run(Data& data, Matrix<float>& predictions) {
	cout << "\nRunning KnnOpenCV";

	knn::data = data;
	X = toMat(data.X);
	Y = toMat(data.Y);

	cv::KNearest knn = cv::KNearest(*X, *Y);

	cv::Mat neighbours;
	cv::Mat results;

	float response = knn.find_nearest(*X, K, &results, 0, &neighbours, 0);
	
	buildPredictions(neighbours, predictions);

	cout << "\t\t ... done\n";
}

void KnnOpenCv::release(){
}